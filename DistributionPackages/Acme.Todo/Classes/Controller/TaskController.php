<?php
namespace Acme\Todo\Controller;

/*
 * This file is part of the Acme.Todo package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use Acme\Todo\Domain\Model\Task;

class TaskController extends ActionController
{

    /**
     * @Flow\Inject
     * @var \Acme\Todo\Domain\Repository\TaskRepository
     */
    protected $taskRepository;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('tasks', $this->taskRepository->findAll());
    }

    /**
     * @param \Acme\Todo\Domain\Model\Task $task
     * @return void
     */
    public function showAction(Task $task)
    {
        $this->view->assign('task', $task);
    }

    /**
     * @return void
     */
    public function newAction()
    {
    }

    /**
     * @param \Acme\Todo\Domain\Model\Task $newTask
     * @return void
     */
    public function createAction(Task $newTask)
    {
        $this->taskRepository->add($newTask);
        $this->addFlashMessage('Created a new task.');
        $this->redirect('index');
    }

    /**
     * @param \Acme\Todo\Domain\Model\Task $task
     * @return void
     */
    public function editAction(Task $task)
    {
        $this->view->assign('task', $task);
    }

    /**
     * @param \Acme\Todo\Domain\Model\Task $task
     * @return void
     */
    public function updateAction(Task $task)
    {
        $this->taskRepository->update($task);
        $this->addFlashMessage('Updated the task.');
        $this->redirect('index');
    }

    /**
     * @param \Acme\Todo\Domain\Model\Task $task
     * @return void
     */
    public function deleteAction(Task $task)
    {
        $this->taskRepository->remove($task);
        $this->addFlashMessage('Deleted a task.');
        $this->redirect('index');
    }
}
