<?php
namespace Acme\Todo\Domain\Model;

/*
 * This file is part of the Acme.Todo package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Task
{

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="StringLength", options={ "maximum"=255 })
     * @var string
     */
    protected $title;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $status;

    /**
     * Constructs this post
     */
    public function __construct()
    {
        $this->date = new \DateTime();
        $this->status = false;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return void
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    /**
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
