<?php
namespace Acme\Todo\Domain\Repository;

/*
 * This file is part of the Acme.Todo package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class TaskRepository extends Repository
{

    // add customized methods here
}
